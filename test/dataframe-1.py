# -*- coding: utf-8 -*-
import pandas as pd

''' 
Procesamiento de los datos for test

Elementos considerados por los GentriFicadores a la hora de decidir
la elección de vivienda (“al decidir vivir acá , ¿cuán importante fue ...?”)

Fuente: Elaboración propia con base en encuesta del proyecto Fondecyt #1130488.
Revista Mexicana de Sociología 79, núm. 2 (abril-junio, 2017): 229-260.
'''

data = {'Muy importante': [55.7, 51.9, 41.1, 34.5, 30.2, 32.9, 16.0, 29.5, 30.7, 51.5, 33.9, 33.9, 15.2, 57.8, 48.3],
        'Importante': [39.1, 42.6, 42.4, 40.2, 33.9, 46.6, 27.0, 39.9, 54.8, 41.4, 45.9, 49.3, 45.1, 39.4, 46.7],
        'Poco importante o nada importante': [5.2, 5.5, 16.5, 25.3, 35.9, 20.5, 57.0, 30.6, 14.5, 7.2, 20.2, 16.9, 39.8, 2.8, 5.0],
        'Total N': [752, 751, 745, 751, 752, 753, 751, 749, 753, 752, 752, 753, 455, 464, 458],
        'Elementos considerados por los Gentrificadores a la hora de decidir': ['Las características de la vivienda (tamaño, calidad y distribución)',
                                                             'El precio de la vivienda',
                                                             'La cercanía a autopistas y medios de transporte',
                                                             'La cercanía al trabajo y/o escuela',
                                                             'Importó la cercanía a familiares',
                                                             'La cercanía a comercio y servicios',
                                                             'La disponibilidad de cafés, restaurantes y lugares de recreación',
                                                             'El tipo de vecinos',
                                                             'La belleza del barrio',
                                                             'La seguridad del barrio',
                                                             'El prestigio del barrio',
                                                             'El equipamiento, como parques, plazas, luminarias, etcétera',
                                                             'Que el condominio no tuviera demasiados vecinos',
                                                             'Que el condominio contara con vigilancia',
                                                             'Que la vivienda quede dentro de un condominio'
                                                            ]
       }

df = pd.DataFrame(data=data)
print(df)


