# Data Science diploma project



## Diplomado en Data Science - Casos de Estudio

Este repositorio contiene los dos casos de estudio realizados como parte del Diplomado de Extensión en Data Science. En ambos casos, se utilizaron técnicas de programación en Python y Data Science para analizar conjuntos de datos y extraer información valiosa.

Las siguientes librerías de Python fueron utilizadas en ambos casos de estudio:

-     numpy
-     pandas
-     matplotlib 
-     seaborn 


### Caso de estudio 1: [nombre del caso de estudio]



### Caso de estudio 2: [nombre del caso de estudio]


