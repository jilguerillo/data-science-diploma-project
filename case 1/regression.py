import numpy as np
import data_processing
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error, r2_score
import matplotlib.pyplot as plt

# Obtener los predictores y el valor medio del hogar
predictors, valorMedioHogar = data_processing.select_predictions()

X = predictors.values
y = valorMedioHogar

# Dividir los datos en conjuntos de entrenamiento y prueba
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Entrenar el modelo utilizando los datos de entrenamiento
model = LinearRegression().fit(X_train, y_train)

# Hacer predicciones utilizando los datos de prueba
y_pred = model.predict(X_test)

# Calcular las métricas de evaluación del modelo
mse = mean_squared_error(y_test, y_pred)
rmse = np.sqrt(mse)
r2 = r2_score(y_test, y_pred)

# Imprimir las métricas de evaluación del modelo
print("MSE:", mse)
print("RMSE:", rmse)
print("R2:", r2)

# Crear un gráfico que muestre la relación entre los valores reales y las predicciones
plt.scatter(y_test, y_pred)
plt.plot([0, 600000], [0, 600000], '--k')
plt.xlabel("Valores reales")
plt.ylabel("Predicciones")
plt.show()

# Análisis de residuos
residuals = y_test - y_pred
plt.scatter(y_pred, residuals)
plt.xlabel("Predicciones")
plt.ylabel("Residuos")
plt.hlines(y=0, xmin=0, xmax=600000, linestyle="--")
plt.show()
