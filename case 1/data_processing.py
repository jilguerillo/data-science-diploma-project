#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Preparación e importación de datos
Caso 1

credit : https://gitlab.com/ssantanar
"""

# Importamos las librerías necesarias
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

def show_how_data_processing():
    
    # Definimos la URL donde se encuentra el archivo .csv que contiene los datos a cargar
    url = 'https://raw.githubusercontent.com/ssantanar/datasets/master/housing_california/california_housing.csv'
    
    # Cargamos los datos en un objeto dataframe utilizando la función read_csv de pandas
    housing_data = pd.read_csv(url)
    
    # Mostramos el número de filas y columnas del dataframe utilizando el método shape
    print(f"El conjunto de datos contiene {housing_data.shape[0]:,} observaciones y {housing_data.shape[1]} columnas.\n")
    
    # Mostramos las primeras 5 filas del dataframe utilizando el método head
    print("Las primeras 15 filas del conjunto de datos son: \n")
    print(housing_data.head(15))
    
    # Imprimimos un mensaje de éxito para indicar que los datos han sido cargados exitosamente
    print("Los datos han sido cargados exitosamente.\n")
    
    # df.describe().round(2) genera un resumen estadístico de las variables numéricas presentes en el dataframe df 
    #y redondea los valores a 2 decimales con round(2).
    housing_data.describe().round(2)
    
    
    ## Mostrar todo el conjunto dataframe
    #pd.set_option('display.max_rows', None)
    #pd.set_option('display.max_columns', None)
    
    #print(housing_data)


    # isna() devuelve una matriz booleana que indica si cada elemento en el dataframe es nulo o no. 
    # La función sum() aplicada sobre esta matriz cuenta el número de valores nulos presentes en cada columna del dataframe
    #housing_data.isna().sum()
    
    # Imprimimos un mensaje con la cantidad de nulos en el dataframe
    print(housing_data.isna().sum())
    
    # Se obtienen las dimensiones del dataframe y se definen las dimensiones de la matriz de subplots para graficar. 
    # Luego se crea un objeto de figura y se generan subplots dentro de ella.
    
    rows, cols = housing_data.shape
    nrows, ncols = (3,3)
    fig, axes = plt.subplots(nrows, ncols, figsize=(18, 12))
    
    
    # Es utilizado la función sns.histplot() directamente sobre el dataframe 
    # añadiendo las líneas verticales utilizando las funciones axvline() y mean() de matplotlib.
    # esto evita recorrer cada columna con el for y usando histplot() genera multiples subplot
    for i, colname in enumerate(housing_data.columns):
        row, col = divmod(i, 3)
        ax = axes[row, col]
        sns.histplot(housing_data[colname], color='lightsteelblue', ax=ax)
        ax.set_title(f'{colname}',fontsize=14)
        ax.set_ylabel('Frecuencia',fontsize=12)
        ax.set_xlabel('')
        ax.axvline(x=housing_data[colname].mean(), color='r')
        ax.axvline(x=housing_data[colname].median(), color='yellowgreen')
    
    """
    Figura de correlación de la data
    
    """    
    ################## INICIO ALTERNATIVA MATRIZ DE CORRELACIÓN ##################     
    # matriz de correlación
    #plt.figure(figsize=(12,9))
    # computamos matriz de correlación
    #corr_matrix = df.corr(method='spearman')
    # esto nos permite graficar sólo la parte superior o inferior de la matriz
    #mask = np.triu(np.ones_like(corr_matrix, dtype=bool))
    # plot
    #sns.heatmap(corr_matrix,mask=mask,annot=True, cmap='coolwarm')
    ################## FIN ALTERNATIVA MATRIZ DE CORRELACIÓN ##################     
    
    
    # se llama a la función corr() directamente sobre el dataframe housing_data  
    # se pasa el resultado como argumento a la función heatmap()
    plt.figure(figsize=(12,9))
    data_correlation = housing_data.corr
    sns.heatmap(data_correlation(method='spearman'), annot=True, cmap='coolwarm')
    
    from itertools import combinations
    
    # número de pares a seleccionar
    n = 10
    
    # lista de todas las columnas
    columnas = list(housing_data.columns)
    
    # diccionario para guardar las correlaciones de cada par de columnas
    correlaciones = {}
    
    # iterar sobre todas las combinaciones de dos columnas
    for par in combinations(columnas, 2):
        # calcular la correlación de Pearson entre las dos columnas
        corr = housing_data[par[0]].corr(housing_data[par[1]])
        # guardar el par de columnas y su correlación en el diccionario
        correlaciones[par] = corr
    
    # ordenar el diccionario por los valores de correlación en orden descendente
    correlaciones_ordenadas = dict(sorted(correlaciones.items(), key=lambda x: x[1], reverse=True))
    
    # seleccionar los primeros n pares de columnas con menor correlación
    pares_seleccionados = list(correlaciones_ordenadas.items())[:n]
    
    # imprimir la lista de pares y sus correlaciones
    for par, correlacion in pares_seleccionados:
        print(f"\n{par}: {correlacion}")
    
    # el color rojo oscuro representa la correlación positiva más fuerte 
    # el color azul oscuro representa la correlación negativa más fuerte 
    # el color blanco indica ausencia de correlación
    # los tonos intermedios de rojo y azul indican correlaciones positivas y negativas de menor magnitud
    
    ## SELECCIÓN DE VARIABLES CON MENOS CORRELACIÓN 
    ## CON RESPECTO A ValorMedioHogar
    
    correlations = housing_data.corr()['ValorMedioHogar'].sort_values()
    predictors = correlations[:5].index.tolist()
    print(f"\n Valores seleccionados por tener menor correlación con ValorMedioHogar : {predictors}")
    
    '''
    Gráfico de dispersión que muestra la relación entre la ubicación geográfica de los inmuebles 
    (variables latitud y longitud) y su precio promedio (ValorMedioHogar).
    
    '''
    
    fig, ax = plt.subplots(figsize=(12, 6))
    im = ax.scatter(x='Longitud', y='Latitud', c='ValorMedioHogar', cmap='coolwarm', alpha=.5, data=housing_data)
    fig.colorbar(im, ax=ax)
    ax.set_xlabel('Longitud')
    ax.set_ylabel('Latitud')
    ax.set_title('Precio promedio de los inmuebles según su latitud y longitud')

def select_predictions():
    # Definimos la URL donde se encuentra el archivo .csv que contiene los datos a cargar
    url = 'https://raw.githubusercontent.com/ssantanar/datasets/master/housing_california/california_housing.csv'

    # Cargamos los datos en un objeto dataframe utilizando la función read_csv de pandas
    housing_data = pd.read_csv(url)
    
    dataframe = housing_data.fillna(0)

    # Extraemos la variable 'predictors'
    correlations = dataframe.corr()['ValorMedioHogar'].sort_values()
    predictors = correlations[:8].index.tolist()

    # Filtramos el dataframe para incluir solo las filas correspondientes a las variables predictoras seleccionadas
    filtered_dataframe = dataframe[predictors]

    # Envía valor target
    valorMedioHogar = dataframe['ValorMedioHogar'].values

    return filtered_dataframe, valorMedioHogar
