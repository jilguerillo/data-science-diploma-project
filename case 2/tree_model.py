#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 31 00:26:11 2023
@author: xubuntu
"""

import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier

# Cargar los datos generales
df = pd.read_csv('https://raw.githubusercontent.com/ssantanar/datasets/master/weather_australia/weatherAUS_s.csv')
df['Fecha'] = pd.to_datetime(df['Fecha'])
df['Mes'] = df.Fecha.dt.month

# Seleccionar los campos con menos correlación
predictors = ['Humedad9am', 'Presion3pm', 'Humedad9am', 'Presion9am', 'Mes', 'Humedad3pm']


# Filtrar el DataFrame con los campos seleccionados
df_filtered = df[predictors]

# Eliminar filas con valores perdidos (NaN)
df_filtered.dropna(inplace=True)

df.isna().sum()
print(df_filtered.columns)

# Separar los datos en características (X) y etiquetas (y)
X = df_filtered
y = df['LlueveManana']

# Dividir los datos en conjuntos de entrenamiento y prueba
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Excluir las columnas DirViento9am y DirViento3pm de X_test si existen en el DataFrame
columns_to_drop = ['DirViento9am', 'DirViento3pm']
X_test = X_test.drop(columns_to_drop, axis=1, errors='ignore')

# Crear el modelo de clasificación (usaremos un árbol de decisiones)
model = DecisionTreeClassifier()

# Entrenar el modelo con los datos de entrenamiento
model.fit(X_train, y_train)

# Predecir la etiqueta "LlueveManana" para los datos de prueba
y_pred = model.predict(X_test)

print("----------->", y_test.shape)

# Calcular la precisión del modelo
accuracy = (y_pred == y_test).mean()
# Mostrar la precisión del modelo
print("Precisión del modelo: {:.2f}".format(accuracy.item()))


# Cargar los datos para predecir y procesado
predict = pd.read_csv("https://raw.githubusercontent.com/ssantanar/datasets/master/weather_australia/weatherAUS_predict.csv", sep=';')
predict = predict.reset_index().rename(columns={'index':'ID'})
predict['Fecha'] = pd.to_datetime(predict['Fecha'])
predict['Mes'] = predict.Fecha.dt.month
predict.isna().sum()

# Verificamos los tipos de campo para validar si todos son NÚMERICOS :
print(predict.dtypes)

# Crear una instancia de LabelEncoder
encoder = LabelEncoder()

# Aplicar LabelEncoder a DirViento9am
predict['DirViento9am_encoded'] = encoder.fit_transform(predict['DirViento9am'])

# Aplicar LabelEncoder a DirViento3pm
predict['DirViento3pm_encoded'] = encoder.fit_transform(predict['DirViento3pm'])



# Eliminar las columnas originales 'DirViento9am' y 'DirViento3pm'
predict.drop(['DirViento9am', 'DirViento3pm'], axis=1, inplace=True)

# Renombrar las columnas 'DirViento9am_encoded' y 'DirViento3pm_encoded' a 'DirViento9am' y 'DirViento3pm'
predict.rename(columns={'DirViento9am_encoded': 'DirViento9am', 'DirViento3pm_encoded': 'DirViento3pm'}, inplace=True)

predict.head()


# Realizar las predicciones utilizando el modelo entrenado
X_predict = predict.drop(['ID', 'Fecha', 'DirViento9am', 'DirViento3pm'], axis=1)


# Verificar si hay valores faltantes en el conjunto de datos de predicción
if X_predict.isnull().values.any():
    print("El conjunto de datos de predicción contiene valores faltantes.")
else:
    # Realizar las predicciones utilizando el modelo entrenado
    y_predict = model.predict(X_predict)

    # Asignar las predicciones al DataFrame "predict"
    predict['LlueveManana'] = y_predict

    # Mostrar el DataFrame con las predicciones
    print(predict)
