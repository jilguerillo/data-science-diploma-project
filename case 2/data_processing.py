#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 29 20:59:46 2023

@author: holi

credit : https://gitlab.com/ssantanar
"""

import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from IPython.display import display

df = pd.read_csv('https://raw.githubusercontent.com/ssantanar/datasets/master/weather_australia/weatherAUS_s.csv')
# transformamos la variable fecha de string a una en formato de fecha
df['Fecha'] = pd.to_datetime(df['Fecha'])
# creamos la columna mes
df['Mes'] = df.Fecha.dt.month

# Devuelve el número de filas en el DataFrame, y :," formatea el número para que tenga separadores de miles.
print(f"Observaciones : {df.shape[0]:,}")

# Devuelve el número de columnas en el DataFrame.
print(f"Columnas      : {df.shape[1]}")
df.head()


# reemplazamos por NaN todos los valores mayores al valor en el percentil 99
df.loc[df.LluviaHoy>np.quantile(df.LluviaHoy, 0.99),'LluviaHoy'] = np.nan
# eliminamos las filas que posean valores perdidos (NaN)
df.dropna(inplace=True)
# estadísticos descriptivos para nuestros datos
df.describe().round(2)

# crea tabla con datos descriptivos
df_descriptive = df.describe().round(2)
display(df_descriptive)

# valores perdidos: notar que no hay valores perdidos
df.isna().sum()
print("Limpieza de valores :", df.isna().sum())

# analisis exploratorio
# plotting Rainfall per Month
plt.figure(figsize=(8,5))
ax = sns.barplot(x = 'Mes', y='LluviaHoy', data=df, color = 'lightsteelblue')
ax.set_title('Lluvia a través de los meses del año',fontsize=15)
ax.set_ylabel('Lluvia (mm)',fontsize=12)
ax.set_xlabel('Mes')
ax.axhline(y=np.mean(df['LluviaHoy']), color='red');

# segundo plot

temp = df.groupby(['Fecha'])[['MinTemp','MaxTemp']].mean().reset_index()
fig, axes = plt.subplots(1,2,figsize=(20,6))
ax = sns.lineplot(data = temp, x='Fecha', y='MinTemp', color='b', ax=axes[0])
ax = sns.lineplot(data = temp, x='Fecha', y='MaxTemp', color='r', ax=axes[0])
ax.set_title('MaxTemp (rojo) y MinTemp (azul) a través de los años',fontsize=15)
ax.set_ylabel('Temperatura',fontsize=14)
ax.set_xlabel('Años',fontsize=14)

# serie para el año 2016
ax = sns.lineplot(data = temp[temp.Fecha.between('2016-01-01','2017-01-01')], x='Fecha', y='MaxTemp', color='r', ax=axes[1])
ax = sns.lineplot(data = temp[temp.Fecha.between('2016-01-01','2017-01-01')], x='Fecha', y='MinTemp', color='b', ax=axes[1])
ax.set_title('MaxTemp (rojo) y MinTemp (azul) para año 2016',fontsize=15)
ax.set_ylabel('Temperatura',fontsize=14)
ax.set_xlabel('Meses',fontsize=14);

# 3 plot
ax = sns.catplot(x = 'LlueveManana', kind = 'count', data = df)
ax.fig.suptitle('Conteo para la variable LlueveManana');

fig, axes = plt.subplots(1,2,figsize=(20,6))
# distribucion para Humedad9am
ax = sns.histplot(data=df, x="Humedad9am", hue="LlueveManana", ax=axes[0])
ax.set_title('Distribución Humedad9am',fontsize=15)
ax.set_ylabel('Frecuencia',fontsize=14)
ax.set_xlabel('Humedad (porcentual)',fontsize=14)
# distribucion para Humedad3pm
ax = sns.histplot(data=df, x="Humedad3pm", hue="LlueveManana", ax=axes[1])
ax.set_title('Distribución Humedad3pm',fontsize=15)
ax.set_ylabel('Frecuencia',fontsize=14)
ax.set_xlabel('Humedad (porcentual)',fontsize=14);

fig, axes = plt.subplots(1,2,figsize=(20,6))
# distribucion para Presion9am
ax = sns.histplot(data=df, x="Presion9am", hue="LlueveManana", ax=axes[0])
ax.set_title('Distribución Presion9am',fontsize=15)
ax.set_ylabel('Frecuencia',fontsize=14)
ax.set_xlabel('Presion (hpa)',fontsize=14)
# distribucion para Presion3pm
ax = sns.histplot(data=df, x="Presion3pm", hue="LlueveManana", ax=axes[1])
ax.set_title('Distribución Presion3pm',fontsize=15)
ax.set_ylabel('Frecuencia',fontsize=14)
ax.set_xlabel('Presion (hpa)',fontsize=14);


fig, axes = plt.subplots(1,2,figsize=(20,6))
# distribucion para MinTemp
ax = sns.histplot(data=df, x="MinTemp", hue="LlueveManana", ax=axes[0])
ax.set_title('Distribución MinTemp',fontsize=15)
ax.set_ylabel('Frecuencia',fontsize=14)
ax.set_xlabel('Temperatura (ºC)',fontsize=14)
# distribucion para MaxTemp
ax = sns.histplot(data=df, x="MaxTemp", hue="LlueveManana", ax=axes[1])
ax.set_title('Distribución MaxTemp',fontsize=15)
ax.set_ylabel('Frecuencia',fontsize=14)
ax.set_xlabel('Temperatura (ºC)',fontsize=14);

# matriz de correlación
plt.figure(figsize=(12,9))
# computamos matriz de correlación
corr_matrix = df.corr(method='spearman')
# esto nos permite graficar sólo la parte superior o inferior de la matriz
mask = np.triu(np.ones_like(corr_matrix, dtype=bool))
# plot
sns.heatmap(corr_matrix,mask=mask,annot=True, cmap='coolwarm');


predict = pd.read_csv("https://raw.githubusercontent.com/ssantanar/datasets/master/weather_australia/weatherAUS_predict.csv", sep=';')
predict = predict.reset_index().rename(columns={'index':'ID'})
predict



